//
// Created by 高山隆之介 on 2019-06-14.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "logger.h"

void logger_print_date(FILE *fp);

enum {
    LOG_INFO, LOG_WARN, LOG_ERROR
};

static const char *level_names[] = {
        "INFO", "WARN", "ERROR"
};

static struct {
    FILE* fp;
    FILE* trace;
    const char *date_pattern;
} Logger;

void logger_init(void) {
    Logger.fp = NULL;
    Logger.trace = NULL;
    Logger.date_pattern = NULL;
}

void logger_set_fp(FILE* fp) {
    Logger.fp = fp;
}

void logger_set_trace(FILE *trace) {
    Logger.trace = trace;
}

FILE* logger_get_fp(void) {
    return Logger.fp;
}

FILE* logger_get_trace(void) {
    return Logger.trace;
}

void logger_set_date_pattern(const char* pattern) {
    Logger.date_pattern = pattern;
}

void logger_log_msgId(const char* msgId, ...) {

    if (Logger.fp == NULL) {
        return;
    }

    // use ProMessageToBuffer to get message from message
    logger_log_msg(msgId);
}

void logger_log_msg(const char* msg, ...) {

    if (Logger.fp == NULL) {
        return;
    }

    logger_print_date(Logger.fp);
    va_list args;
    va_start(args, msg);
    vfprintf(Logger.fp, msg, args);
    va_end(args);
    fprintf(Logger.fp, "\n");
    fflush(Logger.fp);
}

void logger_trace_impl(const char* file, int line, const char* fmt, ...) {

    if (Logger.trace == NULL) {
        return;
    }

    logger_print_date(Logger.trace);

    va_list args;
    va_start(args, fmt);
    vfprintf(Logger.trace, fmt, args);
    va_end(args);
    fprintf(Logger.trace, "\n");
    fflush(Logger.trace);
}


void logger_print_date(FILE* fp) {
    time_t t = time(NULL);
    struct tm *lt = localtime(&t);

    const char *pattern = Logger.date_pattern == NULL ? DEFAULT_DATE_PATTERN : Logger.date_pattern;
    char buf[64];
    buf[strftime(buf, sizeof(buf), pattern, lt)] = '\0';
    fprintf(fp, "%s: ", buf);
}
