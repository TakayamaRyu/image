//
// Created by 高山隆之介 on 2019-06-14.
//

#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#include <stdarg.h>
#include <stdio.h>

#define DEFAULT_DATE_PATTERN "%Y-%m-%d %H:%M:%S"

#define logger_trace(...) logger_trace_impl(__FILE__, __LINE__, __VA_ARGS__)

void logger_init(void);
void logger_set_fp(FILE* fp);
void logger_set_trace(FILE* trace);
void logger_set_date_pattern(const char* pattern);
void logger_log_msgId(const char* msgId, ...);
void logger_log_msg(const char* msg, ...);
void logger_trace_impl(const char* file, int line, const char* fmt, ...);

#endif  // LOGGER_H_INCLUDED
