#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REALLOC_FACTOR 1.5

typedef struct dyn_array_t {
    void* data;
    size_t size;
    size_t capacipy;
    size_t data_size;
} dyn_array;

dyn_array* array_create(size_t size, size_t data_size)
{
    dyn_array* arr = (dyn_array*)malloc(sizeof(dyn_array));
    if (arr == NULL) {
        return NULL;
    }
    arr->data = malloc(size * data_size);
    if (arr->data == NULL) {
        return NULL;
    }
    arr->data_size = data_size;
    arr->size = 0;
    arr->capacipy = size;

    return arr;
}

bool push_to(dyn_array* arr, void* data)
{
    if (arr->size >= arr->capacipy) {
        void* p = NULL;
        size_t new_capacity = arr->capacipy * REALLOC_FACTOR + 1;
        p = realloc(arr->data, arr->data_size * new_capacity);
        if (p == NULL) {
            return false;
        }
        if (p != arr->data) {
            arr->data = p;
            arr->capacipy = new_capacity;
        }
    }
    memcpy(arr->data + (arr->size * arr->data_size), data, arr->data_size);
    arr->size++;

    return true;
}

bool set_to(dyn_array* arr, size_t idx, void* data) { return true; }

void get_by_index(dyn_array* arr, size_t idx, void* data)
{
    memcpy(data, arr->data + (idx * arr->data_size), arr->data_size);
}

int main(int argc, char* argv[])
{
    dyn_array* arr = array_create(1, sizeof(int));
    if(arr == NULL) {
        puts("NULL");
    }
    int aa = 1;
    push_to(arr, &aa);
    aa++;
    push_to(arr, &aa);
    aa++;
    push_to(arr, &aa);
    aa++;
    push_to(arr, &aa);
    aa++;
    push_to(arr, &aa);
    aa++;
    push_to(arr, &aa);
    aa++;

    int d;
    get_by_index(arr, 0, &d);
    printf("%d\n", d);
    get_by_index(arr, 1, &d);
    printf("%d\n", d);
    get_by_index(arr, 2, &d);
    printf("%d\n", d);
    get_by_index(arr, 3, &d);
    printf("%d\n", d);
    return 0;
}
